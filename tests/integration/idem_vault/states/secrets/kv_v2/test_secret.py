from typing import Dict

import pytest


@pytest.mark.asyncio
async def test_secret(hub, ctx):
    pass
    # TODO: Uncomment the following test when the pipeline can initialize Vault properly
    """
    secret_name = "idem-test-kv-v2-secret-" + str(uuid.uuid4())
    path = "secret/idem-test-kv-v2-secret"
    data = {"my-secret": "my-secret-value"}
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.vault.secrets.kv_v2.secret.present(
        ctx=test_ctx, name=secret_name, path=path, data=data
    )
    assert ret["result"], ret["comment"]
    assert f"Would create vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    verify_parameters(
        expect_name=secret_name,
        expect_path=path,
        expect_data=data,
        resource=ret["new_state"],
    )

    ret = await hub.states.vault.secrets.kv_v2.secret.present(
        ctx=ctx, name=secret_name, path=path, data=data
    )
    assert ret["result"], ret["comment"]
    assert f"Created vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    verify_parameters(
        expect_name=secret_name,
        expect_path=path,
        expect_data=data,
        resource=ret["new_state"],
    )

    ctx["test"] = False
    ret = await hub.states.vault.secrets.kv_v2.secret.absent(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted vault.secrets.kv_v2.secret '{secret_name}'." in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    verify_parameters(
        expect_name=secret_name,
        expect_path=path,
        expect_data=None,
        resource=ret["old_state"],
    )
    """


def verify_parameters(
    expect_name: str, expect_path: str, expect_data: Dict or None, resource: Dict
):
    assert expect_name == resource.get("name")
    assert expect_path == resource.get("path")
    assert expect_data == resource.get("data")
