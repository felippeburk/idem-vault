import asyncio
from typing import List
from unittest import mock

import dict_tools.data
import pop.hub
import pytest


@pytest.fixture(scope="module", autouse=True)
def acct_subs() -> List[str]:
    return ["vault"]


@pytest.fixture(scope="module", autouse=True)
def acct_profile() -> str:
    # The name of the profile that will be used for tests
    # Do NOT use production credentials for tests!
    # A profile with this name needs to be defined in the $ACCT_FILE in order to run these tests
    return "test_development_idem_vault"


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="ctx")
async def integration_ctx(hub, acct_subs, acct_profile):
    ctx = dict_tools.data.NamespaceDict(run_name="test", test=False)
    # Uncomment the following two lines if ctx.acct should be loaded from ACCT_KEY and ACCT_FILE
    # await hub.acct.init.unlock(hub.OPT.acct.acct_file, hub.OPT.acct.acct_key)
    # ctx.acct = await hub.acct.init.gather(acct_subs, acct_profile)
    # Add the profile to the account
    ctx.acct = {
        "address": f"http://127.0.0.1:8200",
        "token": "abcdefghijk",
    }
    hub.log.debug(
        f"Set Vault address to {ctx.acct['address']} with token {ctx.acct['token']}"
    )
    yield ctx


@pytest.fixture(scope="module", name="hub")
def integration_hub(event_loop):
    with mock.patch("sys.argv", ["idem_vault"]):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        for dyne in ["idem"]:
            hub.pop.sub.add(dyne_name=dyne)
        hub.pop.config.load(["idem", "acct"], cli="idem", parse_cli=False)

    yield hub
